import os
import subprocess
import contextlib

from flask import current_app as app

from flask_script import Manager
from flask_migrate import MigrateCommand
from files.routes import allowed_file
from files import create_app, db
from files.models import File, meta_file_to_file, check_hash

manager = Manager(create_app)

manager.add_command('db', MigrateCommand)

root_path = os.path.dirname(os.path.abspath(__file__))


def _make_context():
    return dict(app=manager.app, db=db)


@manager.option('-d', '--drop_first', help='Drop tables first?')
def createdb(drop_first=True):
    """Creates the database."""
    db.session.commit()
    if drop_first:
        print("Dropping all databases")
        db.drop_all()
    db.create_all()


@manager.command
def test():
    """Run unit tests"""
    tests = subprocess.Popen(['python', '-m', 'unittest'])
    tests.wait()
    with contextlib.suppress(FileNotFoundError):
        os.remove('files/test.db')


@manager.command
def resettobase():
    db.session.commit()
    db.drop_all()
    db.create_all()


@manager.command
def scan_file_system():
    all_files = {file.guid_file_name: False for file in File.query.all()}
    for dirpath, file_name in walk_uploads_dir():
        print(file_name)
        guid = file_name.rsplit('.', 1)[0]
        file = File.query.filter_by(guid_file_name=guid).one_or_none()
        if not file:
            print("Could not find file in database so adding it from meta info")
            file = meta_file_to_file(location=dirpath, file_name=guid + '.meta')
            if check_hash(file):
                db.session.add(file)
            else:
                print("Error: hash doesn't match, not adding")
        else:
            if not check_hash(file):
                print("Error: hash doesn't match exiting file in db")
            else:
                all_files[guid] = True
    db.session.commit()
    files_that_dont_exist = {k: v for k, v in all_files.items() if not v}
    print("Removing because they are no longer on fs: ")
    print(files_that_dont_exist)
    for guid, v in files_that_dont_exist.items():
        file = File.query.filter_by(guid_file_name=guid).one_or_none()
        if file:
            db.session.delete(file)
    db.session.commit()


def walk_uploads_dir():
    for (dirpath, dirnames, filenames) in os.walk(os.path.join(app.root_path, app.config['UPLOAD_FOLDER'])):
        for x in only_allowed_files(filenames):
            yield dirpath, x


def only_allowed_files(list):
    for l in list:
        if allowed_file(l):
            yield l


if __name__ == '__main__':
    manager.run()
