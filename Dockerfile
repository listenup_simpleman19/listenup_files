FROM listenup/listenup_common:v0.37


RUN apt-get update && apt-get install -y gcc

COPY requirements.txt ${appRoot}
COPY .wheels ${appRoot}/.wheels
RUN pip install --find-links ${appRoot}/.wheels --no-cache-dir -r requirements.txt

ARG SERVICE_NAME
ARG SERVICE_VERSION
ENV SERVICE_NAME $SERVICE_NAME
ENV SERVICE_VERSION $SERVICE_VERSION

COPY . ${appRoot}

EXPOSE 5000
VOLUME ["/usr/src/app/uploads"]

# Start gunicorn
CMD ["./entrypoint.sh"]
