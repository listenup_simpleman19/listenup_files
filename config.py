import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.environ.get('SECRET_KEY',
                                '51f52814-0071-11e6-a247-000ec6c2372c')
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY', SECRET_KEY)
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + basedir + '/files.db'
    LB = os.environ.get('LB', '')
    URL_PREFIX = '/api/files'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOAD_FOLDER = '../uploads'
    EXT_TO_MIME_TYPES = {
        'jpg': 'image/jpeg',
        'jpeg': 'image/jpeg',
        'png': 'image/png',
        'gif': 'image/gif',
        'txt': 'text/plain',
        'pdf': 'application/pdf',
        'mp3': 'audio/mpeg',
        'wav': 'audio/x-wav',
    }
    SERVER_TYPES = {
        'audio': {'mp3', 'wav', 'm4a'},
        'image': {'png', 'jpg', 'jpeg', 'gif'},
        'attachment': {'txt', 'pdf', 'ppt', 'pptx'},
        'all': {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'mp3', 'wav', 'm4a'},
    }
    SERVER_TYPE = os.environ.get('SERVER_TYPE', None)


class DevelopmentConfig(Config):
    DEBUG = True
    SESSION_COOKIE_SECURE = False
    TEMPLATES_AUTO_RELOAD = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + basedir + '/files.db'
    UPLOAD_FOLDER = '../uploads'
    SERVER_TYPE = 'all'


class ProductionConfig(Config):
    pass


class TestingConfig(ProductionConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + basedir + '/test.db'
    UPLOAD_FOLDER = './tmp'


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
