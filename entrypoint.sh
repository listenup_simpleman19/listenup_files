#!/bin/bash

export LISTENUP_APP_CONFIG=production
export SERVER_TYPE=all

if [ -f /.passwords ]; then
  source /.passwords
fi
if [ -f /.config ]; then
  source /.config
fi

DEBUG=$1
if [[ -z "$DEBUG" ]]; then
   DEBUG=""
else
    shift
fi

if [[ "$DEBUG" == "--debug" ]]; then
    WSGI="wsgi_debug"
    CMD_ARGS="--bind=0.0.0.0:5000 --workers=1"
else
    WSGI="wsgi"
    CMD_ARGS="--bind=0.0.0.0:5000 --workers=6"
fi

# Create or upgrade database
python manage.py db upgrade

# Scan File System
python manage.py scan_file_system &

GUNICORN_CMD_ARGS=${CMD_ARGS} /usr/local/bin/gunicorn ${WSGI}:app