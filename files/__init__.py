import os

from flask import Flask, request, session
from flask_migrate import Migrate
from config import config
from files import models
from files.models import db
from files.marshaller import get_server_guid

migrate = Migrate()

print(get_server_guid())


def create_app(config_name=None):
    if config_name is None:
        config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')
        print("Starting up in: " + config_name)
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.host = '0.0.0.0'

    # Initialize flask extensions
    db.init_app(app)
    migrate.init_app(app=app, db=db)

    # Register web application routes
    from .routes import main as main_blueprint
    from listenup_common.stats import stats
    app.register_blueprint(main_blueprint)
    app.register_blueprint(stats, url_prefix='/api/files/server')

    return app
