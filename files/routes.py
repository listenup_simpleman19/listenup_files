import os
from flask import Blueprint, render_template, session, redirect, url_for, request, flash, current_app as app, g, send_from_directory, make_response
from werkzeug.utils import secure_filename
from listenup_common.logging import get_logger
from files.api_result import ApiResult
from files.errors import ApiException, ApiMessage
from files.models import File, hash_file
from files.marshaller import get_hostname, get_url
from files.models import db
import json

main = Blueprint('main', __name__, url_prefix='/api/files')

root_path = os.path.dirname(os.path.abspath(__file__))

logger = get_logger(__name__)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in app.config['SERVER_TYPES'][app.config['SERVER_TYPE']]


@main.route('/upload_file', methods=['PUT'])
def upload():
    if 'file' not in request.files:
        logger.error("No file in request to upload file")
        raise ApiException(message="No file in request", status=403)
    logger.info("Receiving a new uploaded file from external upload")
    file = request.files['file']
    filename = request.form.get('filename', None)
    if not filename:
        filename = file.filename
        logger.info("Could not find filename in form so using filename of upload: %s", filename)
    filename = secure_filename(filename)
    if not file.filename:
        raise ApiException(message="File name was not specified", status=403)
    if file and allowed_file(filename):
        logger.debug("File was in allowed file list: %s", filename)
        location = os.path.join(app.root_path, app.config['UPLOAD_FOLDER'])
        new_file = File(file_name=filename, location=location)
        db.session.add(new_file)
        filename_and_path = os.path.join(new_file.location, new_file.guid_file_name + '.' + new_file.file_extension)
        file.save(filename_and_path)
        new_file.hash = hash_file(filename_and_path)
        new_file.uploaded = True
        if new_file.file_extension in app.config['EXT_TO_MIME_TYPES']:
            new_file.type = app.config['EXT_TO_MIME_TYPES'][new_file.file_extension]
        db.session.commit()
        new_file.write_metafile()
        logger.info("Added new file guid: %s", new_file.guid_file_name)
        return ApiResult(value=new_file.to_dict(), status=200).to_response()
    else:
        logger.error("Invalid file was uploaded: %s", filename)
        raise ApiException(message="Invalid file type", status=403)


# TODO protect this for internal apps only
@main.route('/internal_upload_file', methods=['PUT'])
def internal_upload():
    if 'file' not in request.files:
        raise ApiException(message="No file in request", status=403)
    logger.info("Receiving a new uploaded file from internal upload")
    file = request.files['file']
    file_info = request.form.get('info')
    if isinstance(file_info, str):
        file_info = json.loads(file_info)
    if file_info:
        logger.info("File is coming from an existing internal file: %s", file_info)
    else:
        logger.info("File is an internal upload but the info file was not found")
    if not file.filename:
        raise ApiException(message="File name was not specified", status=403)
    if file and (allowed_file(file.filename) or (file_info and file_info.get("guid_file_name", None))): # TODO check type of file being duplicated
        filename = secure_filename(file.filename)
        location = os.path.join(app.root_path, app.config['UPLOAD_FOLDER'])
        if file_info:
            logger.info("Building file using existing info:" + str(file_info))
            if File.query.filter_by(guid_file_name=file_info['guid_file_name']).one_or_none():
                raise ApiException(message="File with that guid already exists on server", status=403) # TODO is this the right status??
            new_file = File(file_name=filename, location=location, info=file_info)
        else:
            new_file = File(file_name=filename, location=location)
        db.session.add(new_file)
        filename_and_path = os.path.join(new_file.location, new_file.guid_file_name + '.' + new_file.file_extension)
        file.save(filename_and_path)
        new_file.hash = hash_file(filename_and_path)
        new_file.uploaded = True
        if new_file.file_extension in app.config['EXT_TO_MIME_TYPES']:
            new_file.type = app.config['EXT_TO_MIME_TYPES'][new_file.file_extension]
        db.session.commit()
        new_file.write_metafile()
        return ApiResult(value=new_file.to_dict(), status=200).to_response()
    else:
        raise ApiException(message="Invalid file type", status=403)


@main.route('/check/<path:guid>')
def check_for_file(guid):
    if db.session.query(File.query.filter(File.guid_file_name == guid).exists()).scalar():
        return ApiMessage(message="File found", status=200).to_response()
    else:
        return ApiMessage(message="File not found", status=404).to_response()


@main.route('/file/<path:guid>')
def get_file(guid):
    file = File.query.filter_by(guid_file_name=guid).one_or_none()
    if file:
        return ApiResult(value=file.to_dict(), status=200).to_response()
    else:
        return ApiException(message="Could not find file", status=404).to_response()


@main.route('/files')
def files_on_server():
    logger.warn("Getting all files on server")
    files = File.query.all()
    files_array = [f.to_dict() for f in files]
    return ApiResult(value=files_array, status=200).to_response()


@main.route('/uploads/<path:guid>')
def serve_file(guid):
    file = File.query.filter_by(guid_file_name=guid).one_or_none()
    if not file:
        raise ApiException(message="Could not find file", status=404)
    full_path = file.path
    if file:
        if file.type == "" and file.file_extension in app.config['EXT_TO_MIME_TYPES']:
            file.type = app.config['EXT_TO_MIME_TYPES'][file.file_extension]
        return send_from_directory(directory=full_path, filename=file.filename, mimetype=file.type)


@main.route('/info')
def info():
    hostname = get_hostname()
    url = get_url()
    if hostname and url:
        info_dict = {
                'hostname': hostname,
                'base_url_endpoint': '/api/files',
                'health_check_endpoint': '/api/files/health_check',
                'server_type': app.config['SERVER_TYPE'],
                'url': url,
        }
        return ApiResult(value=info_dict).to_response()
    else:
        raise ApiException(message="Could not determine hostname or url", status=503)


@main.route('/health_check')
def health_check():
    return ApiResult(value={'status': 'healthy'}, status=208).to_response()


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
