from datetime import date, datetime
import requests
from files.models import File
from flask import current_app as app
import json
import os
import contextlib
import socket
from config import basedir


def default(o):
    if type(o) is date or type(o) is datetime:
        return o.isoformat()


def register_server_with_marshaller():
    resp = requests.post(
        url='http://listenup_marshaller:5000/api/marshaller/register',
        json={
            'base_url': 'http://files1.local:5000:5000/api/files',
            'health_check': 'http://files1.local:5000/api/files/health_check',
            'server_type': app.config['SERVER_TYPE'],
        })
    if resp.status_code == 202 and resp.json():
        print("Registered with server: " + resp.json())
        server_guid = resp.json().get('server_guid')
        os.environ['SERVER_GUID'] = str(server_guid)
        write_guid_to_file(server_guid)
        return server_guid
    return None


def write_guid_to_file(guid):
    with open(os.path.join(basedir, 'server_guid'), 'w') as f:
        f.write(guid)


def read_guid_from_file():
    guid = None
    with contextlib.suppress(FileNotFoundError):
        with open(os.path.join(basedir, 'server_guid'), 'r') as f:
            guid = f.readline().strip()
    return guid


def get_server_guid():
    server_guid = os.environ.get('SERVER_GUID', None)
    if not server_guid:
        server_guid = read_guid_from_file()
    return server_guid


def get_hostname():
    hostname = None
    with contextlib.suppress(FileNotFoundError):
        with open('/hostname', 'r') as f:
            hostname = f.readline().strip()
    if hostname:
        return socket.getfqdn(hostname)
    else:
        return None


def get_url():
    url = None
    with contextlib.suppress(FileNotFoundError):
        with open('/url', 'r') as f:
            url = f.readline().strip()
    return url
