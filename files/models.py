from flask import current_app as app
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import uuid
import json
import os
import hashlib


BLOCKSIZE = 65536

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False,
                                   default=datetime.utcnow)


class File(BaseModel):
    __tablename__ = 'file'
    original_file_name = db.Column(db.String(255), nullable=True)
    file_extension = db.Column(db.String(30), nullable=False)
    guid_file_name = db.Column(db.String(32), nullable=False, unique=True)
    location = db.Column(db.String(4096), nullable=False)
    uploaded = db.Column(db.Boolean(), default=False, nullable=False)
    type = db.Column(db.String(300), default="", nullable=False)
    quality = db.Column(db.String(300), default="", nullable=False)
    hash = db.Column(db.String(65), default="", nullable=False)

    def __init__(self, file_name, location, file_type="", quality="", info=None):
        if info:
            self.guid_file_name = info['guid_file_name']
            self.original_file_name = info['original_file_name']
            self.file_extension = info['file_extension']
            self.type = info['type']
            self.quality = info['quality']
        else:
            self.guid_file_name = File._guid()
            split_name = file_name.rsplit('.', 1)
            self.original_file_name = split_name[0]
            self.file_extension = split_name[1]
            self.type = file_type
            self.quality = quality
        self.location = location

    @staticmethod
    def _guid():
        return str(uuid.uuid4()).replace('-', '')

    @property
    def path(self):
        return self.location

    @property
    def filename(self):
        return self.guid_file_name + '.' + self.file_extension

    @property
    def metapath(self):
        return self.path

    @property
    def metaname(self):
        return self.guid_file_name + ".meta"

    @property
    def metafile_contents(self):
        return {
            "original_file_name": self.original_file_name,
            "file_extension": self.file_extension,
            "guid_file_name": self.guid_file_name,
            "location": self.location,
            "creation_timestamp": str(self.creation_timestamp.timestamp()),
            "type": self.type,
            "quality": self.quality,
            "hash": self.hash,
        }

    @property
    def url(self):
        return app.config['LB'] + app.config['URL_PREFIX'] + '/uploads/' + self.guid_file_name

    def write_metafile(self):
        with open(os.path.join(self.metapath, self.metaname), 'w') as f:
            json.dump(self.metafile_contents, f, indent=4)

    def to_dict(self):
        return {
            'id': self.id,
            'original_file_name': self.original_file_name,
            'file_extension': self.file_extension,
            'guid_file_name': self.guid_file_name,
            'creation_timestamp': self.creation_timestamp,
            'url': self.url,
            'uploaded': self.uploaded,
            'type': self.type,
            'quality': self.quality,
            'hash': self.hash,
        }


def meta_file_to_file(location, file_name):
    file = File(location=location, file_name="dont.care")
    with open(os.path.join(location, file_name), 'r') as f:
        data = json.load(f)
        for k, v in data.items():
            if k == 'creation_timestamp':
                v = datetime.utcfromtimestamp(float(v))
            setattr(file, k, v)
    file.uploaded = True
    return file


def check_hash(file: File):
    hash = hash_file(os.path.join(file.path, file.filename))
    if hash == file.hash:
        return True
    else:
        return False


def hash_file(filename_and_path):
    hasher = hashlib.sha256()
    with open(filename_and_path, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()
